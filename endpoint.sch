<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="microchip">
<description>&lt;b&gt;Microchip PIC Microcontrollers and other Devices&lt;/b&gt;&lt;p&gt;
Based on the following sources :
&lt;ul&gt;
&lt;li&gt;Microchip Data Book, 1993
&lt;li&gt;THE EMERGING WORLD STANDARD, 1995/1996
&lt;li&gt;Microchip, Technical Library CD-ROM, June 1998
&lt;li&gt;www.microchip.com
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="QFN16-4X4">
<description>&lt;b&gt;16-Lead Plastic Quad Flat, No Lead Package (ML) - 4x4x0.9mm Body [QFN]&lt;/b&gt;&lt;p&gt;
Source: http://ww1.microchip.com/downloads/en/DeviceDoc/41203E.pdf</description>
<wire x1="-1.95" y1="1.925" x2="1.95" y2="1.925" width="0.1016" layer="21"/>
<wire x1="1.95" y1="-1.95" x2="-1.95" y2="-1.95" width="0.1016" layer="21"/>
<wire x1="-1.95" y1="1.925" x2="-1.95" y2="-1.95" width="0.1016" layer="51"/>
<wire x1="1.95" y1="-1.95" x2="1.95" y2="1.925" width="0.1016" layer="51"/>
<smd name="1" x="-2" y="0.975" dx="0.8" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="2" x="-2" y="0.325" dx="0.8" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="3" x="-2" y="-0.325" dx="0.8" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="4" x="-2" y="-0.975" dx="0.8" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="EXP" x="0" y="0" dx="2.5" dy="2.5" layer="1" stop="no" cream="no"/>
<smd name="5" x="-0.975" y="-2" dx="0.8" dy="0.35" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="6" x="-0.325" y="-2" dx="0.8" dy="0.35" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="7" x="0.325" y="-2" dx="0.8" dy="0.35" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="8" x="0.975" y="-2" dx="0.8" dy="0.35" layer="1" rot="R90" stop="no" cream="no"/>
<smd name="9" x="2" y="-0.975" dx="0.8" dy="0.35" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="10" x="2" y="-0.325" dx="0.8" dy="0.35" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="11" x="2" y="0.325" dx="0.8" dy="0.35" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="12" x="2" y="0.975" dx="0.8" dy="0.35" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="13" x="0.975" y="2" dx="0.8" dy="0.35" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="14" x="0.325" y="2" dx="0.8" dy="0.35" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="15" x="-0.325" y="2" dx="0.8" dy="0.35" layer="1" rot="R270" stop="no" cream="no"/>
<smd name="16" x="-0.975" y="2" dx="0.8" dy="0.35" layer="1" rot="R270" stop="no" cream="no"/>
<text x="-1.95" y="2.6" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.95" y="-3.9" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.95" y1="1.475" x2="-1.525" y2="1.925" layer="51"/>
<rectangle x1="-1.3" y1="-1.3" x2="1.3" y2="1.3" layer="29"/>
<rectangle x1="-1.175" y1="-1.175" x2="1.175" y2="1.175" layer="31"/>
<rectangle x1="-2.45" y1="0.75" x2="-1.55" y2="1.2" layer="29"/>
<rectangle x1="-2.375" y1="0.825" x2="-1.625" y2="1.125" layer="31"/>
<rectangle x1="-2.45" y1="0.1" x2="-1.55" y2="0.55" layer="29"/>
<rectangle x1="-2.375" y1="0.175" x2="-1.625" y2="0.475" layer="31"/>
<rectangle x1="-2.45" y1="-0.55" x2="-1.55" y2="-0.1" layer="29"/>
<rectangle x1="-2.375" y1="-0.475" x2="-1.625" y2="-0.175" layer="31"/>
<rectangle x1="-2.45" y1="-1.2" x2="-1.55" y2="-0.75" layer="29"/>
<rectangle x1="-2.375" y1="-1.125" x2="-1.625" y2="-0.825" layer="31"/>
<rectangle x1="-1.425" y1="-2.225" x2="-0.525" y2="-1.775" layer="29" rot="R90"/>
<rectangle x1="-1.35" y1="-2.15" x2="-0.6" y2="-1.85" layer="31" rot="R90"/>
<rectangle x1="-0.775" y1="-2.225" x2="0.125" y2="-1.775" layer="29" rot="R90"/>
<rectangle x1="-0.7" y1="-2.15" x2="0.05" y2="-1.85" layer="31" rot="R90"/>
<rectangle x1="-0.125" y1="-2.225" x2="0.775" y2="-1.775" layer="29" rot="R90"/>
<rectangle x1="-0.05" y1="-2.15" x2="0.7" y2="-1.85" layer="31" rot="R90"/>
<rectangle x1="0.525" y1="-2.225" x2="1.425" y2="-1.775" layer="29" rot="R90"/>
<rectangle x1="0.6" y1="-2.15" x2="1.35" y2="-1.85" layer="31" rot="R90"/>
<rectangle x1="1.55" y1="-1.2" x2="2.45" y2="-0.75" layer="29" rot="R180"/>
<rectangle x1="1.625" y1="-1.125" x2="2.375" y2="-0.825" layer="31" rot="R180"/>
<rectangle x1="1.55" y1="-0.55" x2="2.45" y2="-0.1" layer="29" rot="R180"/>
<rectangle x1="1.625" y1="-0.475" x2="2.375" y2="-0.175" layer="31" rot="R180"/>
<rectangle x1="1.55" y1="0.1" x2="2.45" y2="0.55" layer="29" rot="R180"/>
<rectangle x1="1.625" y1="0.175" x2="2.375" y2="0.475" layer="31" rot="R180"/>
<rectangle x1="1.55" y1="0.75" x2="2.45" y2="1.2" layer="29" rot="R180"/>
<rectangle x1="1.625" y1="0.825" x2="2.375" y2="1.125" layer="31" rot="R180"/>
<rectangle x1="0.525" y1="1.775" x2="1.425" y2="2.225" layer="29" rot="R270"/>
<rectangle x1="0.6" y1="1.85" x2="1.35" y2="2.15" layer="31" rot="R270"/>
<rectangle x1="-0.125" y1="1.775" x2="0.775" y2="2.225" layer="29" rot="R270"/>
<rectangle x1="-0.05" y1="1.85" x2="0.7" y2="2.15" layer="31" rot="R270"/>
<rectangle x1="-0.775" y1="1.775" x2="0.125" y2="2.225" layer="29" rot="R270"/>
<rectangle x1="-0.7" y1="1.85" x2="0.05" y2="2.15" layer="31" rot="R270"/>
<rectangle x1="-1.425" y1="1.775" x2="-0.525" y2="2.225" layer="29" rot="R270"/>
<rectangle x1="-1.35" y1="1.85" x2="-0.6" y2="2.15" layer="31" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="PIC16F688">
<wire x1="-33.02" y1="10.16" x2="35.56" y2="10.16" width="0.254" layer="94"/>
<wire x1="35.56" y1="10.16" x2="35.56" y2="-10.16" width="0.254" layer="94"/>
<wire x1="35.56" y1="-10.16" x2="-33.02" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-33.02" y1="-10.16" x2="-33.02" y2="10.16" width="0.254" layer="94"/>
<text x="-33.02" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="-33.02" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="-35.56" y="7.62" length="short" direction="pwr"/>
<pin name="RA4/AN3/!T1G!/OSC2/CLKOUT" x="-35.56" y="2.54" length="short"/>
<pin name="RA3/!MCLR!/VPP" x="-35.56" y="0" length="short"/>
<pin name="RA5/T1CKI/OSC1/CLKIN" x="-35.56" y="5.08" length="short" direction="in"/>
<pin name="RA0/AN0/C1IN+/ICSPDAT/ULPWU" x="38.1" y="5.08" length="short" rot="R180"/>
<pin name="RA1/AN1/C1IN-/VREF/ICSPCLK" x="38.1" y="2.54" length="short" rot="R180"/>
<pin name="RA2/AN2/T0CKI/INT/C1OUT" x="38.1" y="0" length="short" rot="R180"/>
<pin name="RC2/AN6" x="38.1" y="-7.62" length="short" rot="R180"/>
<pin name="RC1/AN5/C2IN" x="38.1" y="-5.08" length="short" rot="R180"/>
<pin name="RC0/AN4/C2IN+" x="38.1" y="-2.54" length="short" rot="R180"/>
<pin name="RC5/RX/DT" x="-35.56" y="-2.54" length="short"/>
<pin name="RC4/C2OUT/TX/CK" x="-35.56" y="-5.08" length="short"/>
<pin name="RC3/AN7" x="-35.56" y="-7.62" length="short"/>
<pin name="VSS" x="38.1" y="7.62" length="short" direction="pwr" rot="R180"/>
</symbol>
<symbol name="EXPOSED">
<pin name="EXP" x="0" y="-2.54" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PIC16F688" prefix="IC">
<description>&lt;b&gt;14-Pin Flash-Based, 8-Bit CMOS Microcontrollers with nanoWatt Technology&lt;/b&gt;&lt;p&gt;
Source: http://ww1.microchip.com/downloads/en/DeviceDoc/41203E.pdf</description>
<gates>
<gate name="G$1" symbol="PIC16F688" x="0" y="0"/>
<gate name="EXP" symbol="EXPOSED" x="0" y="-10.16" addlevel="request"/>
</gates>
<devices>
<device name="" package="QFN16-4X4">
<connects>
<connect gate="EXP" pin="EXP" pad="EXP"/>
<connect gate="G$1" pin="RA0/AN0/C1IN+/ICSPDAT/ULPWU" pad="12"/>
<connect gate="G$1" pin="RA1/AN1/C1IN-/VREF/ICSPCLK" pad="11"/>
<connect gate="G$1" pin="RA2/AN2/T0CKI/INT/C1OUT" pad="10"/>
<connect gate="G$1" pin="RA3/!MCLR!/VPP" pad="3"/>
<connect gate="G$1" pin="RA4/AN3/!T1G!/OSC2/CLKOUT" pad="2"/>
<connect gate="G$1" pin="RA5/T1CKI/OSC1/CLKIN" pad="1"/>
<connect gate="G$1" pin="RC0/AN4/C2IN+" pad="9"/>
<connect gate="G$1" pin="RC1/AN5/C2IN" pad="8"/>
<connect gate="G$1" pin="RC2/AN6" pad="7"/>
<connect gate="G$1" pin="RC3/AN7" pad="6"/>
<connect gate="G$1" pin="RC4/C2OUT/TX/CK" pad="5"/>
<connect gate="G$1" pin="RC5/RX/DT" pad="4"/>
<connect gate="G$1" pin="VDD" pad="16"/>
<connect gate="G$1" pin="VSS" pad="13"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="PIC16F688-I/ML" constant="no"/>
<attribute name="OC_FARNELL" value="1579588" constant="no"/>
<attribute name="OC_NEWARK" value="56K7280" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="myrra_v2">
<packages>
<package name="1">
<wire x1="-13.35" y1="15.85" x2="13.35" y2="15.85" width="0.127" layer="21"/>
<wire x1="13.35" y1="15.85" x2="13.35" y2="-15.85" width="0.127" layer="21"/>
<wire x1="13.35" y1="-15.85" x2="-13.35" y2="-15.85" width="0.127" layer="21"/>
<wire x1="-13.35" y1="-15.85" x2="-13.35" y2="15.85" width="0.127" layer="21"/>
<pad name="1" x="-10" y="10" drill="1" diameter="2.54"/>
<pad name="5" x="-10" y="-10" drill="1" diameter="2.54"/>
<pad name="7" x="10" y="-5" drill="1" diameter="2.54"/>
<pad name="9" x="10" y="5" drill="1" diameter="2.54"/>
<text x="-6.985" y="1.27" size="1.27" layer="21">&gt;NAME</text>
<text x="-6.985" y="-2.54" size="1.27" layer="21">&gt;VALUE</text>
<text x="-10.16" y="12.7" size="1.27" layer="21">In</text>
<text x="-10.16" y="-13.97" size="1.27" layer="21">In</text>
<text x="5.08" y="-5.715" size="1.27" layer="21">+V</text>
<text x="5.08" y="4.445" size="1.27" layer="21">0V</text>
</package>
</packages>
<symbols>
<symbol name="1">
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<text x="-7.62" y="10.16" size="1.778" layer="97">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.778" layer="97">&gt;VALUE</text>
<text x="-6.985" y="4.445" size="1.27" layer="94">In</text>
<text x="-6.985" y="-5.715" size="1.27" layer="94">In</text>
<text x="4.445" y="-3.175" size="1.27" layer="94">+V</text>
<text x="4.445" y="1.905" size="1.27" layer="94">0V</text>
<pin name="1" x="-10.16" y="5.08" length="short" direction="in"/>
<pin name="5" x="-10.16" y="-5.08" length="short" direction="in"/>
<pin name="7" x="10.16" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="9" x="10.16" y="2.54" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SINGLE" uservalue="yes">
<description>&lt;b&gt;One Output&lt;/b&gt;&lt;br/&gt;
Input voltage range: 85 to 265 V AC or 85 to 370 V DC&lt;br/&gt;
Input frequency: 47 to 440 Hz&lt;br/&gt;
Output voltage accuracy (full load ): &amp;#177; 2%&lt;br/&gt;
Line output voltage variation: &amp;#177; 0.3%&lt;br/&gt;
Load output voltage variation: &amp;#177; 0.5%&lt;br/&gt;
Temperature range: -25 - TA (°C)&lt;br/&gt;
No load input power: &amp;lt; 200mW
&lt;pre&gt;&lt;b&gt;regulated:&lt;/b&gt;
Reference  Output  Current  Power  Efficiency  Ta
           Volts   mA       Watts  %           °C
47121       3.3    750      2.5    65          +70
47122       5      550      2.75   68          +70
47123       9      270      2.5    72          +70
47124      12      210      2.5    74          +70
47125      15      170      2.5    75          +70
47126      24      110      2.5    77          +70

47151       3.3   1350      4.2    65          +50
47152       5      900      4.5    68          +50
47153       9      550      5      72          +50
47154      12      420      5      75          +50
47155      15      320      5      76          +50
47156      24      220      5      79          +50


&lt;b&gt;non regulated:&lt;/b&gt;
Reference  Output  Current  Power  Efficiency  Ta
           Volts   mA       Watts  %           °C
47114      12      200      2.4    74          +70

47133       9      360      3.2    73          +70
47134      12      270      3.2    75          +70
47136      24      130      3.2    80          +70

47163       9      560      5*     73          +50
47164      12      420      5*     75          +50
47166      24      210      5*     80          +50&lt;/pre&gt;
&lt;b&gt;* Nota:&lt;/b&gt; Power up to 5.4W is possible with input voltage &amp;gt; 97 V AC&lt;br/&gt;
&lt;br/&gt;
Source: http://www.myrra.com</description>
<gates>
<gate name="G$1" symbol="1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="microchip" deviceset="PIC16F688" device=""/>
<part name="TRANSFORMER" library="myrra_v2" deviceset="SINGLE" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="58.42" y="43.18"/>
<instance part="TRANSFORMER" gate="G$1" x="12.7" y="78.74"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
